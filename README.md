**目的 : 利用python程式控制，當樹莓派接收到紅外線遙控器的訊號時，反應出相對應的結果。**

# 硬體需求
* RPi x1
* 紅外線感應器 x1 (本範例使用"PL-IRM0101-X")
* 紅外線遙控器 x1 (本範例使用arduino的紅外線套件模組)
* 線材

# 接線作業
**註 : 此範例使用"PL-IRM0101-X"，各型號腳位不同，應查清後再接線。**
* 紅外線感測器應有三根針腳，分別為Signal、GND、Vcc。
    * Signal : 此範例使用GPIO 2(即Pin#3)，應記住此腳位，要與後面的設定配合。
    * GND : 接地。(如:Pin#6)
    * Vcc : 該型號需要5V來驅動，因此接Pin#4。(各型號不同，請上網查詢datasheet)

# 樹莓派前置作業

### 1. 安裝lirc函式庫：

    sudo apt-get install lirc
    sudo apt-get install python-lirc

### 2. 修改檔案：

* 修改 /etc/modules
    
        sudo nano /etc/modules
    
    * 增加兩行文字

        > lirc_dev

        > lirc_rpi gpio_in_pin=2
        
        * gpio_in_pin=2 為紅外線接收器的訊號腳位，編號為GPIO.BCM，要與實際接線相同。


* 修改 /boot/config.txt
    
        sudo nano /boot/config.txt

    * 增加一行文字

        > dtoverlay=lirc-rpi,gpio_in_pin=2
        
        * gpio_in_pin=2 同上。


* 修改 /etc/lirc/lirc_options.conf
    
        sudo nano /etc/lirc/lirc_options.conf

    * 修改driver及device的參數

        > driver = default
        
        > device = /dev/lirc0
        
* 修改完成，請重新開機
    
        sudo reboot

# 下載並運行此範例

### 1. 下載至樹梅派：

* 先回到桌面：
    
        cd
        cd Desktop

* 下載至桌面：

        git clone https://gitlab.com/dave851221/IR-receiver.git

* 進入config資料夾：

        cd IR-receiver/config/

### 2. config資料夾(各個設定檔)：

* hardware.conf
    
    * 將此檔案複製於 /etc/lirc/ 底下。

            sudo cp hardware.conf /etc/lirc/hardware.conf
            
* lircrc
    
    * 將此檔案複製於 /etc/lirc/ 底下。

            sudo cp lircrc /etc/lirc/lircrc
        
        * 註 : 此檔處理各按鈕事件，名稱需配合/etc/lirc/lircd.conf.d/底下的設定檔(.conf)。
        
* lircd.conf

    * 將此檔案複製於 /etc/lirc/lircd.conf.d/ 底下。

            sudo cp lircd.conf /etc/lirc/lircd.conf.d/lircd.conf

        * 註 : 此檔處理各按鈕的頻率識別，各個紅外線遙控器將有所不同(若此範例無法順利運行，有可能是遙控器不同)。
        * 註 : 可透過commend : "irrecord -d /dev/lirc0 ~/lircd.conf" 來產生。
        * 特別注意 : 若在 /etc/lirc 內找不到 lircd.conf.d 資料夾(為舊版lirc)，則將此檔案放置於/etc/lirc底下，並覆蓋原本的 lircd.conf 即可。
        

### 3. 執行python：

* IRreceiver.py

    * 函式庫，需與 main.py 放於同個路徑。

* main.py

    * 使用 python2.7 撰寫，若使用 python3 則將所有的 print 改為函式的寫法即可。
    * 若以上均確認完成，即可運行。
    
            python main.py